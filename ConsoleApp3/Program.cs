﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            HtmlWeb p = new HtmlWeb();
            var doc = p.Load(@"https://twitter.com/dailygametips");
            var nodes = doc.DocumentNode.SelectNodes("//table[@class='tweet  ']");
            List<Tweet> tweets = new List<Tweet>();

            // Get User Data first (Id and Name)
            var avatarNode = doc.DocumentNode.SelectSingleNode("//td[@class='avatar']");
            Regex regex = new Regex("/([\\d]+)/?");
            string userId = regex.Match(avatarNode.InnerHtml).Value.Trim('/');            
            regex = new Regex(@"(?<=\balt="")[^""]*");
            string username = regex.Match(avatarNode.InnerHtml).Value;

            // Run through tweets
            foreach (var node in nodes)
            {
                bool isRetweet = false;
                var spanNode = node.SelectSingleNode(".//span[@class='context']");
                if (spanNode != null && spanNode.InnerHtml.Contains("retweeted"))
                {
                    isRetweet = true;
                }

                string msg = string.Empty;
                string tweetId = string.Empty;

                var msgNode = node.SelectSingleNode(".//div[@class='tweet-text']");
                if (msgNode != null)
                {
                    tweetId = msgNode.GetAttributeValue("data-id", string.Empty);
                    msg = msgNode.InnerText.Trim();
                }

                string timestamp = GetTweetDatetime(p, username.Replace(" ",string.Empty), tweetId);
                //var time = DateTime.ParseExact(timestamp, // 3:48 AM - 24 Mar 2017
                //                                "h:mm tt - dd MMM yyyy",
                //                                CultureInfo.InvariantCulture);

                tweets.Add(new Tweet(userId, username, tweetId, msg, isRetweet, timestamp));
            }
        }

        public static string GetTweetDatetime(HtmlWeb p,string username, string tweetId)
        {
            var tweetHtml = p.Load(@"https://twitter.com/"+ username + "/status/" + tweetId);

            var metaNode = tweetHtml.DocumentNode.SelectSingleNode(".//div[@class='metadata']");

            return metaNode.InnerText.Replace("\r","").Replace("\n", "").Trim();
        }

        class Tweet
        {
            public Tweet(string userId, string username, string id, string message, bool isRetweet, string timestamp)
            {
                UserId = userId;
                UserName = username;
                TweetId = id;
                Message = message;
                IsRetweet = isRetweet;
                Timestamp = timestamp;
            }

            string Message { get; set; }
            bool IsRetweet { get; set; }
            string TweetId { get; set; }
            string Timestamp { get; set; }
            string UserId { get; set; }
            string UserName { get; set; }
        }
    }

}
